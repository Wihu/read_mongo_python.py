import pprint
import pandas as pd
import numpy as np
from datetime import date
import matplotlib.pyplot as plt
from pymongo import MongoClient

client = MongoClient()

db = client.dbsgp
sampling = db.sampling
food = db.food
mortality = db.mortality
medication = db.medication
harvest = db.harvest
transfer = db.transfer
parameters = db.parameters
treatment = db.treatment
sowing = db.sowing
historytank = db.historytank
lote = db.lote

#  Esta funcion nos permite a partir del numero del lote 1,2,3... tener a la mano
# todos los datos del muestreo incluido los lotes trasladados
tablagral = []
nf = 1
y = 0
nro = input('escribe el numero de lote: ')
while (y < nf):
    for doc in transfer.find({'numLote': nro}):
        nf = len(doc['target'])
        idLote = doc['IdLote']
        idtanks = doc['targetTanks'][y]
        idtanksori = doc['targetTanks'][y] #sirve pr conservr el nro del tnqe cundo son vrios trldos
        targetlotid = doc['target'][idtanks]['currentLoteId']       
        for esp in lote.find({'_id': idLote}):
            idespecie = esp['id_name_species']
        
        if y == 0:
            for docn in sowing.find({'idLote': idLote}):
                idLote = docn['idLote'] #se puede borrr y
                daten = docn['date']
                daynbr = 1
                idtanks = docn['id_num_tanks']
                tablagral.append([idLote, daten, 0, daynbr, idtanks, idespecie])
                                       
                                       
            for docs in sampling.find({'idLote': idLote}):
                idLote = docs['idLote'] #se puede borr
                date = docs['date']
                average = docs['average']
                daynbr = abs(date - daten).days
                tablagral.append([idLote, date, average, daynbr, idtanks, idespecie])
                
                
            for doc in sampling.find({'idLote': targetlotid}):
                idLote = doc['idLote'] 
                date = doc['date']
                average = doc['average']
                idtanks = idtanksori
                daynbr = abs(date - daten).days
                tablagral.append([idLote, date, average, daynbr, idtanks, idespecie])
                
        else:
            
            for doc in sampling.find({'idLote': targetlotid}):
                idLote = doc['idLote']
                date = doc['date']
                average = doc['average']
                daynbr = abs(date - daten).days
                tablagral.append([idLote, date, average, daynbr, idtanks, idespecie])
                 
                
        y +=1

a = pd.DataFrame(tablagral)
a.columns = ['LoteID', 'Date','Weight', 'Days', 'Tank', 'Especie']
plt.plot(a.Days, a.Weight, 'ro')#, a.Days, a.Weight, 'bs', a.Days, a.Weight, 'g^' )
plt.xlabel('Dias desde Siembra')
plt.ylabel('Peso del Muestreo')
plt.title('Peso VS Tiempo')
plt.grid()
a
