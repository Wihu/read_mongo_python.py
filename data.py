from pymongo import MongoClient
client = MongoClient()
db = client.dbsgp
import pprint
import pandas as pd
from datetime import date

sampling = db.sampling
food = db.food
mortality = db.mortality
medication = db.medication
harvest = db.harvest
transfer = db.transfer
parameters = db.parameters
treatment = db.treatment

tabla = []
for doc in sampling.find():
    idLote = doc['idLote']
    date = doc['date']
    average = doc['average']
    tabla.append([idLote, date, average])
    
pd.DataFrame(tabla)